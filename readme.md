Commands that I executed

#Create thescrapy project
scrapy startproject quotes_spider

#Generate a simple spider
scrapy genspider quotes quotes.toscrape.com

#List the existing spiders
scrapy list

#Open shell for developers
scrapy shell 'quotes.toscrape.com'

#Extract the data in 3 differents output formats in file
scrapy crawl quotes -o items.xml
scrapy crawl quotes -o items.json
scrapy crawl quotes -o items.csv
