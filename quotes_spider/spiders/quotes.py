# -*- coding: utf-8 -*-
import os
import glob

from openpyxl import Workbook
import scrapy
from scrapy.loader import ItemLoader

from quotes_spider.items import QuotesSpiderItem
class QuotesSpider(scrapy.Spider):
    name = 'quotes'
    allowed_domains = ['quotes.toscrape.com']
    start_urls = ['http://quotes.toscrape.com/']

    def parse(self, response):
        item = QuotesSpiderItem()
        quotes = response.xpath('//*[@class="quote"]')
        for quote in quotes:
            text = quote.xpath('.//*[@class="text"]/text()').extract_first()
            author = quote.xpath('.//*[@class="author"]/text()').extract_first()
            #tags = quote.xpath('.//*[@class="tags"]/a/text()').extract()
            tags = quote.xpath('.//*[@itemprop="keywords"]/@content').extract_first()

            item['text'] = text
            item['author'] = author
            item['tags'] = tags

            yield item

#            yield {'Text':text,
#                    'Author': author,
#                    'Tags':tags}
        #This yield over the paginator ang gets all the data            
        next_page_url = response.xpath('//*[@class="next"]/a/@href').extract_first()
        absolute_next_page_url = response.urljoin(next_page_url)
        yield scrapy.Request(absolute_next_page_url)
    
    def close(self, reason):
        csv_file = max(glob.iglob('*.csv'), key=os.path.getctime)

        wb = Workbook()
        ws = wb.active

        with open(csv_file, 'r') as f:
            for row in csv.reader(f):
                ws.append(row)

        wb.save(csv_file.replace('.csv', '') + '.xlsx')