# -*- coding: utf-8 -*-
import scrapy
from scrapy.loader import ItemLoader

from quotes_spider.items import QuotesSpiderItem
class QuotesSpider(scrapy.Spider):
    name = 'quotes'
    allowed_domains = ['quotes.toscrape.com']
    start_urls = ['http://quotes.toscrape.com/']

    def parse(self, response):
        l = QuotesSpiderItem()
        quotes = response.xpath('//*[@class="quote"]')
        for quote in quotes:
            text = quote.xpath('.//*[@class="text"]/text()').extract_first()
            author = quote.xpath('.//*[@class="author"]/text()').extract_first()
            #tags = quote.xpath('.//*[@class="tags"]/a/text()').extract()
            tags = quote.xpath('.//*[@itemprop="keywords"]/@content').extract_first()

            l['text'] = text
            l['author'] = author
            l['tags'] = tags

            yield l

#            yield {'Text':text,
#                    'Author': author,
#                    'Tags':tags}
        #This yield over the paginator ang gets all the data            
        next_page_url = response.xpath('//*[@class="next"]/a/@href').extract_first()
        absolute_next_page_url = response.urljoin(next_page_url)
        yield scrapy.Request(absolute_next_page_url)